#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Akash Kothawale <akash@decached.com>
#
# Distributed under terms of the MIT license.

from pipetown import quench_town_forever

def test_quench_town_forever():
    old_state = [75, 60, 80, 95, 45, 50, 70, 40, 35, 65]
    inflow = [20, 30, 15, 0, 35, 25, 25, 20, 45, 15]
    new_state = [95, 90, 95, 30, 30, 10, 10, 30, 30, 30]

    assert quench_town_forever(old_state, inflow) == new_state

# Pipe Town

## Problem Statement

There are 10 pipes that transport water to a small town. Each pipe has an upper
limit of the amount of water it can carry (for simplicity assume that a pipe
cannot carry more than 100L of water). All the pipes together rest on a piece of
land that is really soft and can collapse at any time. The risk of the land
collapsing is very high when the total weight of the water being carried by all
the pipes is greater than or equal to the weight carrying threshold of the land.
On the other hand, if the total weight of the water in all the pipes goes below
a defined lower threshold, then the people in the town aren't going to have
enough water. How are you going to ensure that the land underneath the pipe
doesn't collapse, and all the people in the town get enough water to live?

Write a function `quenchTownForever(...)` that takes the following arguments,

1. An array of values, each representing the number of litres of water being
pumped into a pipe. The values are going to be integers.

`quenchTownForever(...)` should return the new state of all the pipes.

The pipes (array of ints representing current weight), the weight carrying
threshold (upper threshold) and the least amount of water possible (lower
threshold) are going to be predefined constants.

Note: Assume that weight of 1L of water is 1kg. However, don't get hung up on
this.

```
long upperThreshold = 650; //litres
long lowerThreshold = 350; //litres
long maxWaterPerPipe = 100; //litres
int numberOfPipes = 10;
long[] currentStateOfPipes = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

// returns the updated currentStateOfPipes[...]
long[] quenchTownForever(int[] inflowOrOutflow) {
    
}
```

Only when the total weight of all the pipes exceeds the upperThreshold, then you
can unload water from only one pipe at a time (By unloading water, I mean
reducing the value for that pipe in `currentStateOfPipes[..]`). The amount of
water that is unloaded is left to you, but make sure that the same amount of
water is unloaded every time.
